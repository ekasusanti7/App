package datacenter.programmer_es.simda_apk;

/**
 * Created by Programmer_ES on 5/3/2017.
 */
public class Config {

//get data untuk masing-masing spinner
    public static final String GET_DATA_URUSAN="http://10.0.3.2/odbcsimda/get_data_urusan.php";
    public static final String GET_DATA_BIDANG="http://10.0.3.2/odbcsimda/get_data_bidang.php";
    public static final String GET_DATA_UNIT="http://10.0.3.2/odbcsimda/get_data_unit.php";
    public static final String GET_DATA_SUB_UNIT="http://10.0.3.2/odbccsimda/get_data_urusan.php";
    public static final String GET_DATA_PROGRAM="http://10.0.3.2/odbcsimda/get_data_urusan.php";
    public static final String GET_DATA_KEGIATAN="http://10.0.3.2/odbcsimda/get_data_urusan.php";


  //data urusan
    public static final String KEY_ID_URUSAN="Kd_Urusan";
    public static final String KEY_NM_URUSAN="Nm_Urusan";

  //data bidang
    public static final String KEY_ID_BIDANG="Kd_Bidang";
    public static final String KEY_NM_BIDANG="Nm_Bidang";

  //data unit
    public static final String KEY_ID_UNIT="Kd_Unit";
    public static final String KEY_NM_UNIT="Nm_Unit";

 //data sub unit
    public static final String KEY_ID_SUB_UNIT="Kd_Sub";
    public static final String KEY_NM_SUB="Nm_Sub";

 //data program
    public static final String KEY_ID_PROG="Kd_Prog";
    public static final String KEY_NM_PROG="Ket_Program";

 //data kegiatan
    public static final String KEY_ID_KEG="Kd_Keg";
    public static final String KEY_NM_KEG="Ket_Kegiatan";

   public static final String EMP_ID = "empid";
  //
    public static final String JSON_ARRAY="result";

}
