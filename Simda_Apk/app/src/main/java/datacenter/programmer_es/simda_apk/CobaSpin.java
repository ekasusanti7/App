package datacenter.programmer_es.simda_apk;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Programmer_ES on 5/8/2017.
 */

public class CobaSpin extends AppCompatActivity implements OnItemSelectedListener {
    int hour, minute, mYear,mMonth, mDay;
    static final int TIME_DIALOG_ID = 0;
    static final int DATE_DIALOG_ID = 1;
    static final int DATE_DIALOG_ID2 = 2;
    private EditText txtDate, txtDate2;
    private EditText txtTime;
    private TextView txtKd;
    private String[] arrMonth = {"01","02","03","04","05","06","07","08","09","10","11","12"};
    //private MaterialSpinner spinUrusan,spinBidang,spinUnit,spinSub,spinProg,spinKegiatan,spinJenis,spinLevel;
    private Spinner spinUrusan,spinBidang,spinUnit,spinSub,spinProg,spinKegiatan,spinJenis,spinLevel;
    private Button btnKeluar,btnLihat;
    ArrayList <LUrusan> Urusan;
    ArrayList <LBidang> Bidang;
    ArrayList <LUnit> Unit;
    //ArrayList <LCoba> Urusan;
    ProgressDialog pDialog;

    private JSONArray result;
    // API urls
    // Url to get urusan
    private String URL_GET_URUSAN = "http://10.0.3.2/odbcsimda/get_data_urusan.php";
    private String URL_GET_BIDANG = "http://10.0.3.2/odbcsimda/get_data_bidang.php?Kd_Urusan=1";
    private String URL_GET_UNIT = "http://10.0.3.2/odbcsimda/get_data_unit.php?Kd_Urusan=1&&Kd_Bidang=1";

    //private String URL_GET_URUSAN = "http://10.0.3.2/odbcsimda/get_data_peralatan.php";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_laporan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.inc_menuutama);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtDate = (EditText) findViewById(R.id.txtDate);
        txtDate2 = (EditText) findViewById(R.id.txtDate2);
        //txtTime = (EditText) findViewById(R.id.txtTime);
        txtKd =(TextView) findViewById(R.id.txtKd_Urusan);
        spinUrusan = (Spinner) findViewById(R.id.spinnerUrusan);
        spinBidang = (Spinner) findViewById(R.id.spinnerBidang);
        spinUnit = (Spinner) findViewById(R.id.spinnerUnit);
        spinSub = (Spinner) findViewById(R.id.spinnerSub);
        spinProg = (Spinner) findViewById(R.id.spinnerProgram);
        spinKegiatan = (Spinner) findViewById(R.id.spinnerKegiatan);
        spinLevel = (Spinner) findViewById(R.id.spinnerLevel);
        spinJenis = (Spinner) findViewById(R.id.spinnerJenis);

        Urusan = new ArrayList<LUrusan>();
        Bidang = new ArrayList<LBidang>();
        Unit = new ArrayList<LUnit>();
        //Urusan = new ArrayList<LCoba>();

        // spinner item select listener
        //spinUrusan.MaterialSpinner.OnItemSelectedListener this);

        //btnKeluar= (Button) findViewById(R.id.btnKeluar);
        btnLihat=(Button) findViewById(R.id.btnLihat);

        // get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        txtDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub
                showDialog(DATE_DIALOG_ID);
                return true;
            }
        });
        txtDate2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                showDialog(DATE_DIALOG_ID2);
                return true;

            }
        });


//        txtTime.setOnTouchListener(new OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View arg0, MotionEvent arg1) {
//                // TODO Auto-generated method stub
//                showDialog(TIME_DIALOG_ID);
//                return true;
//            }
//        });

        //getSpinnerUrusan();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // fetching all categories
                new GetData().execute();
            }
        });
    }

    /**
     * Adding spinner data
     * */
    private void populateSpinner() {
        List<String> lables = new ArrayList<String>();
        String kd;
        for (int i = 0; i < Urusan.size(); i++) {
            lables.add(Urusan.get(i).getNm_Urusan());
            //kd.add (Urusan.get(i).getKd_Urusan());
        }
        final List<String> bdg = new ArrayList<String>();
        for(int i =0; i < Bidang.size(); i++){
            bdg.add(Bidang.get(i).getNm_Bidang());
        }
        List<String> unt = new ArrayList<String>();
        for (int i =0; i < Unit.size(); i++){
            unt.add(Unit.get(i).getNm_Unit());
        }

//        for (int i = 0; i < Urusan.size(); i++) {
//            lables.add(Urusan.get(i).getUsername());
//        }

        // Creating adapter for spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);
        ArrayAdapter<String> spinnerAdapBdg = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bdg);
        ArrayAdapter<String> spinnerAdapUnt = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, unt);

        // Drop down layout style - list view with radio button
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapBdg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapUnt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinUrusan.setAdapter(spinnerAdapter);
        spinBidang.setAdapter(spinnerAdapBdg);
        spinUnit.setAdapter(spinnerAdapUnt);
//        spinUrusan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                String selection = spinUrusan.getSelectedItem().toString();
//                ArrayAdapter<String> selection_subcat = new ArrayAdapter<String>(CobaSpin.this,
//                        android.R.layout.simple_spinner_dropdown_item, bdg.getNm_Bidang(selection));
//                spinBidang.setAdapter(selection_subcat);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here (optional)
//            }
//
//        });
    }
    private String getKd(int position){
        String kd="";
        try {
            //Getting object of given index
            JSONObject json = result.getJSONObject(position);

            //Fetching name from that object
            kd = json.getString(Config.KEY_ID_URUSAN);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Returning the name
        return kd;
    }

    /**
     * Async task to get all food categories
     * */
    private class GetData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CobaSpin.this);
            pDialog.setMessage("Loading Data...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            ServiceHandler jsonParser = new ServiceHandler();
            String json = jsonParser.makeServiceCall(URL_GET_URUSAN, ServiceHandler.GET);
            String jsonBdg = jsonParser.makeServiceCall(URL_GET_BIDANG,ServiceHandler.GET);
            String jsonUnt = jsonParser.makeServiceCall(URL_GET_UNIT,ServiceHandler.GET);
            Log.e("Response: ", "> " + json);
            Log.e("Response Bidang : ", "> "+jsonBdg);
            Log.e("Response Unit : ", "> "+jsonUnt);
            if (json != null) {
                try {
                    JSONObject jsonObj = new JSONObject(json);
                    if (jsonObj != null) {
                        JSONArray urusan = jsonObj
                                .getJSONArray("urusan");
                        Log.e("Response= ", "> " + urusan);
                        for (int i = 0; i < urusan.length(); i++) {
                            JSONObject catObj = (JSONObject) urusan.get(i);
                            LUrusan cat = new LUrusan(catObj.getInt("Kd_Urusan"),
                                    catObj.getString("Nm_Urusan"));

                            Urusan.add(cat);
                        }
//                            JSONArray user = jsonObj
//                                    .getJSONArray("user");
//                            Log.e("Response: ", "> " + user);
//                            for (int i = 0; i < user.length(); i++) {
//                                JSONObject catObj = (JSONObject) user.get(i);
//                                LCoba cat = new LCoba(catObj.getInt("id_user"),
//                                        catObj.getString("username"));
//                                Urusan.add(cat);
//                            }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("JSON Data", "Didn't receive any data from server!");
            }
            if (jsonBdg != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonBdg);
                    if (jsonObj != null) {
                        JSONArray bidang = jsonObj
                                .getJSONArray("bidang");
                        Log.e("Response= ", "> " + bidang);
                        for (int i = 0; i < bidang.length(); i++) {
                            JSONObject catObj = (JSONObject) bidang.get(i);
                            LBidang cat = new LBidang(catObj.getInt("Kd_Urusan"),catObj.getInt("Kd_Bidang"),
                                    catObj.getString("Nm_Bidang"));

                            Bidang.add(cat);
                        }
//                            JSONArray user = jsonObj
//                                    .getJSONArray("user");
//                            Log.e("Response: ", "> " + user);
//                            for (int i = 0; i < user.length(); i++) {
//                                JSONObject catObj = (JSONObject) user.get(i);
//                                LCoba cat = new LCoba(catObj.getInt("id_user"),
//                                        catObj.getString("username"));
//                                Urusan.add(cat);
//                            }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("JSON Data", "Didn't receive any data from server!");
            }
            if (jsonUnt != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonBdg);
                    if (jsonObj != null) {
                        JSONArray unit = jsonObj
                                .getJSONArray("unit");
                        Log.e("Response= ", "> " + unit);
                        for (int i = 0; i < unit.length(); i++) {
                            JSONObject catObj = (JSONObject) unit.get(i);
                            LUnit cat = new LUnit(catObj.getInt("Kd_Urusan"),catObj.getInt("Kd_Bidang"),
                                    catObj.getInt("Kd_Unit"),
                                    catObj.getString("Nm_Bidang"));

                            Unit.add(cat);
                        }
//                            JSONArray user = jsonObj
//                                    .getJSONArray("user");
//                            Log.e("Response: ", "> " + user);
//                            for (int i = 0; i < user.length(); i++) {
//                                JSONObject catObj = (JSONObject) user.get(i);
//                                LCoba cat = new LCoba(catObj.getInt("id_user"),
//                                        catObj.getString("username"));
//                                Urusan.add(cat);
//                            }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("JSON Data", "Didn't receive any data from server!");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            populateSpinner();



        }
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        Toast.makeText(
                getApplicationContext(),
                parent.getItemAtPosition(position).toString() + " Selected",
                Toast.LENGTH_LONG).show();
    }


    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch (id) {
//            case TIME_DIALOG_ID:
//                return new TimePickerDialog(
//                        this, mTimeSetListener, hour, minute, true);
            case DATE_DIALOG_ID:
                return new DatePickerDialog(
                        this, mDateSetListener, mYear, mMonth, mDay);
            case DATE_DIALOG_ID2:
                return new DatePickerDialog(
                        this, mDateSetListener2, mYear, mMonth, mDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    //String sdate = arrMonth[mMonth] + " " + LPad(mDay + "", "0", 2) + ", " + mYear;
                    //String stdate = LPad(mDay + "","0",2) +"-"+arrMonth[mMonth]+"-"+mYear;
                    String stdate = mYear+"-"+arrMonth[mMonth]+"-"+LPad(mDay+"","0",2);
                    txtDate.setText(stdate);
                }
            };
    private DatePickerDialog.OnDateSetListener mDateSetListener2 =
            new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    //String sdate = arrMonth[mMonth] + " " + LPad(mDay + "", "0", 2) + ", " + mYear;
                    //String stdate = LPad(mDay + "","0",2) +"-"+arrMonth[mMonth]+"-"+mYear;
                    String stdate = mYear+"-"+arrMonth[mMonth]+"-"+LPad(mDay+"","0",2);
                    txtDate2.setText(stdate);
                }
            };
//    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
//            new TimePickerDialog.OnTimeSetListener()
//            {
//                public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour)
//                {
//                    hour = hourOfDay;
//                    minute = minuteOfHour;
//                    String stime = LPad(""+hour, "0", 2) + ":"+ LPad(""+hour, "0", 2);
//                    txtTime.setText(stime);
//                }
//            };

    private static String LPad(String schar, String spad, int len) {
        String sret = schar;
        for (int i = sret.length(); i < len; i++) {
            sret = spad + sret;
        }
        return new String(sret);
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_home,menu);
        getMenuInflater().inflate(R.menu.menu_site,menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent =new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_site:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bpkd.kotamobagukota.go.id"));
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
