package datacenter.programmer_es.simda_apk;

        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

        import android.app.DatePickerDialog;
        import android.app.Dialog;
        import android.app.ProgressDialog;
        import android.app.TimePickerDialog;
        import android.content.Intent;
        import android.net.Uri;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.support.v7.widget.Toolbar;
        import android.util.Log;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.MotionEvent;
        import android.view.View;
        import android.view.View.OnTouchListener;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.DatePicker;
        import android.widget.EditText;
        import android.widget.ImageButton;
        import android.widget.Spinner;
        import android.widget.TimePicker;
        import android.widget.Toast;
        import android.widget.AdapterView;
        import android.widget.AdapterView.OnItemSelectedListener;

        import com.android.volley.AuthFailureError;
        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;
        import com.jaredrummler.materialspinner.MaterialSpinner;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

public class Laporan extends AppCompatActivity implements OnItemSelectedListener{
    int hour, minute, mYear,mMonth, mDay;
    static final int TIME_DIALOG_ID = 0;
    static final int DATE_DIALOG_ID = 1;
    static final int DATE_DIALOG_ID2 = 2;
    private EditText txtDate, txtDate2;
    private EditText txtTime;
    private String[] arrMonth = {"01","02","03","04","05","06","07","08","09","10","11","12"};
    //private MaterialSpinner spinUrusan,spinBidang,spinUnit,spinSub,spinProg,spinKegiatan,spinJenis,spinLevel;
    private Spinner spinUrusan,spinBidang,spinUnit,spinSub,spinProg,spinKegiatan,spinJenis,spinLevel;
    private Button btnKeluar,btnLihat;
    private List<String> listSpinnerUrusan = new ArrayList<String>();
    private List<String> listSpinnerBidang = new ArrayList<String>();
    private List<String> listSpinnerUnit = new ArrayList<String>();
    private List<String> listSpinnerSub = new ArrayList<String>();
    private List<String> listSpinnerProg = new ArrayList<String>();
    private List<String> listSpinnerKeg = new ArrayList<String>();
    private List<String> listSpinnerJenis = new ArrayList<String>();
    private List<String> listSpinnerLevel = new ArrayList<String>();
    private List<String> listKdUrusan = new ArrayList<String>();
    private List<String> listKdBidang = new ArrayList<String>();
    private List<String> listKdUnit = new ArrayList<String>();
    private List<String> listKdSub = new ArrayList<String>();
    private List<String> listKdProg = new ArrayList<String>();
    private List<String> listKdKeg = new ArrayList<String>();
    private List<String> listKdJenis = new ArrayList<String>();
    private List<String> listKdLevel = new ArrayList<String>();
    ArrayList <LUrusan> Urusan;
    //ArrayList <LCoba> Urusan;
    ProgressDialog pDialog;

    // API urls
    // Url to get urusan
    private String URL_GET_URUSAN = "http://10.0.3.2/odbcsimda/get_data_urusan.php";
    //private String URL_GET_URUSAN = "http://10.0.3.2/odbcsimda/get_data_peralatan.php";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_laporan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.inc_menuutama);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtDate = (EditText) findViewById(R.id.txtDate);
        txtDate2 = (EditText) findViewById(R.id.txtDate2);
        //txtTime = (EditText) findViewById(R.id.txtTime);
        spinUrusan = (Spinner) findViewById(R.id.spinnerUrusan);
        spinBidang = (Spinner) findViewById(R.id.spinnerBidang);
        spinUnit = (Spinner) findViewById(R.id.spinnerUnit);
        spinSub = (Spinner) findViewById(R.id.spinnerSub);
        spinProg = (Spinner) findViewById(R.id.spinnerProgram);
        spinKegiatan = (Spinner) findViewById(R.id.spinnerKegiatan);
        spinLevel = (Spinner) findViewById(R.id.spinnerLevel);
        spinJenis = (Spinner) findViewById(R.id.spinnerJenis);

        Urusan = new ArrayList<LUrusan>();
       //Urusan = new ArrayList<LCoba>();

        // spinner item select listener
        //spinUrusan.MaterialSpinner.OnItemSelectedListener this);

        //btnKeluar= (Button) findViewById(R.id.btnKeluar);
        btnLihat=(Button) findViewById(R.id.btnLihat);

        // get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        txtDate.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub
                showDialog(DATE_DIALOG_ID);
                return true;
            }
        });
        txtDate2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                showDialog(DATE_DIALOG_ID2);
                return true;

            }
        });

//        txtTime.setOnTouchListener(new OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View arg0, MotionEvent arg1) {
//                // TODO Auto-generated method stub
//                showDialog(TIME_DIALOG_ID);
//                return true;
//            }
//        });

        //getSpinnerUrusan();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // fetching all categories
                new GetUrusan().execute();
            }
        });
    }

    /**
     * Adding spinner data
     * */
    private void populateSpinner() {
        List<String> lables = new ArrayList<String>();

        for (int i = 0; i < Urusan.size(); i++) {
            lables.add(Urusan.get(i).getNm_Urusan());
        }
//        for (int i = 0; i < Urusan.size(); i++) {
//            lables.add(Urusan.get(i).getUsername());
//        }

        // Creating adapter for spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinUrusan.setAdapter(spinnerAdapter);
    }


    /**
     * Async task to get all food categories
     * */
        private class GetUrusan extends AsyncTask<Void, Void, Void> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(Laporan.this);
                pDialog.setMessage("Menampilkan data urusan..");
                pDialog.setCancelable(false);
                pDialog.show();

            }

            @Override
            protected Void doInBackground(Void... arg0) {
                ServiceHandler jsonParser = new ServiceHandler();
                String json = jsonParser.makeServiceCall(URL_GET_URUSAN, ServiceHandler.GET);

                Log.e("Response: ", "> " + json);

                if (json != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        if (jsonObj != null) {
                            JSONArray urusan = jsonObj
                                    .getJSONArray("urusan");
                            Log.e("Response: ", "> " + urusan);
                            for (int i = 0; i < urusan.length(); i++) {
                                JSONObject catObj = (JSONObject) urusan.get(i);
                                LUrusan cat = new LUrusan(catObj.getInt("Kd_Urusan"),
                                        catObj.getString("Nm_Urusan"));
                                Urusan.add(cat);

                            }
//                            JSONArray user = jsonObj
//                                    .getJSONArray("user");
//                            Log.e("Response: ", "> " + user);
//                            for (int i = 0; i < user.length(); i++) {
//                                JSONObject catObj = (JSONObject) user.get(i);
//                                LCoba cat = new LCoba(catObj.getInt("id_user"),
//                                        catObj.getString("username"));
//                                Urusan.add(cat);
//                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.e("JSON Data", "Didn't receive any data from server!");
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (pDialog.isShowing())
                    pDialog.dismiss();
                populateSpinner();

            }
        }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        Toast.makeText(
                getApplicationContext(),
                parent.getItemAtPosition(position).toString() + " Selected" ,
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }


    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch (id) {
//            case TIME_DIALOG_ID:
//                return new TimePickerDialog(
//                        this, mTimeSetListener, hour, minute, true);
            case DATE_DIALOG_ID:
                return new DatePickerDialog(
                        this, mDateSetListener, mYear, mMonth, mDay);
            case DATE_DIALOG_ID2:
                return new DatePickerDialog(
                        this, mDateSetListener2, mYear, mMonth, mDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    //String sdate = arrMonth[mMonth] + " " + LPad(mDay + "", "0", 2) + ", " + mYear;
                    //String stdate = LPad(mDay + "","0",2) +"-"+arrMonth[mMonth]+"-"+mYear;
                    String stdate = mYear+"-"+arrMonth[mMonth]+"-"+LPad(mDay+"","0",2);
                    txtDate.setText(stdate);
                }
            };
    private DatePickerDialog.OnDateSetListener mDateSetListener2 =
            new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    //String sdate = arrMonth[mMonth] + " " + LPad(mDay + "", "0", 2) + ", " + mYear;
                    //String stdate = LPad(mDay + "","0",2) +"-"+arrMonth[mMonth]+"-"+mYear;
                    String stdate = mYear+"-"+arrMonth[mMonth]+"-"+LPad(mDay+"","0",2);
                    txtDate2.setText(stdate);
                }
            };
//    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
//            new TimePickerDialog.OnTimeSetListener()
//            {
//                public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour)
//                {
//                    hour = hourOfDay;
//                    minute = minuteOfHour;
//                    String stime = LPad(""+hour, "0", 2) + ":"+ LPad(""+hour, "0", 2);
//                    txtTime.setText(stime);
//                }
//            };

    private static String LPad(String schar, String spad, int len) {
        String sret = schar;
        for (int i = sret.length(); i < len; i++) {
            sret = spad + sret;
        }
        return new String(sret);
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_home,menu);
        getMenuInflater().inflate(R.menu.menu_site,menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent =new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_site:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bpkd.kotamobagukota.go.id"));
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
