package datacenter.programmer_es.simda_apk;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Calendar;

/**
 * Created by Programmer_ES on 5/3/2017.
 */
public class TataUsaha extends AppCompatActivity {
    int hour, minute, mYear,mMonth, mDay;
    static final int TIME_DIALOG_ID = 0;
    static final int DATE_DIALOG_ID = 1;
    private EditText txtDate;
    private EditText txtTime;
    private String[] arrMonth = {"01","02","03","04","05","06","07","08","09","10","11","12"};
    private Spinner spinUrusan,spinBidang,spinUnit,spinSub;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tatausaha);

        Toolbar toolbar = (Toolbar) findViewById(R.id.inc_menuutama);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtDate = (EditText) findViewById(R.id.txtDate);
        //txtTime = (EditText) findViewById(R.id.txtTime);
        // get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        txtDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub
                showDialog(DATE_DIALOG_ID);
                return true;
            }
        });

//        txtTime.setOnTouchListener(new OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View arg0, MotionEvent arg1) {
//                // TODO Auto-generated method stub
//                showDialog(TIME_DIALOG_ID);
//                return true;
//            }
//        });

    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch (id) {
//            case TIME_DIALOG_ID:
//                return new TimePickerDialog(
//                        this, mTimeSetListener, hour, minute, true);
            case DATE_DIALOG_ID:
                return new DatePickerDialog(
                        this, mDateSetListener, mYear, mMonth, mDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    //String sdate = arrMonth[mMonth] + " " + LPad(mDay + "", "0", 2) + ", " + mYear;
                    //String stdate = LPad(mDay + "","0",2) +"-"+arrMonth[mMonth]+"-"+mYear;
                    String stdate = mYear+"-"+arrMonth[mMonth]+"-"+LPad(mDay+"","0",2);
                    txtDate.setText(stdate);
                }
            };

//    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
//            new TimePickerDialog.OnTimeSetListener()
//            {
//                public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfHour)
//                {
//                    hour = hourOfDay;
//                    minute = minuteOfHour;
//                    String stime = LPad(""+hour, "0", 2) + ":"+ LPad(""+hour, "0", 2);
//                    txtTime.setText(stime);
//                }
//            };

    private static String LPad(String schar, String spad, int len) {
        String sret = schar;
        for (int i = sret.length(); i < len; i++) {
            sret = spad + sret;
        }
        return new String(sret);
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_home,menu);
        getMenuInflater().inflate(R.menu.menu_site,menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent =new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_site:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bpkd.kotamobagukota.go.id"));
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
