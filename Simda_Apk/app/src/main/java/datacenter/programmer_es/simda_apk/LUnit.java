package datacenter.programmer_es.simda_apk;

/**
 * Created by Programmer_ES on 5/12/2017.
 */
public class LUnit {
    private int Kd_Urusan;
    private int Kd_Bidang;
    private int Kd_Unit;
    private String Nm_Unit;
    public LUnit(){}
    public LUnit(int Kd_Urusan, int Kd_Bidang, int Kd_Unit, String Nm_Unit){
        this.Kd_Urusan = Kd_Urusan;
        this.Kd_Bidang = Kd_Bidang;
        this.Kd_Unit = Kd_Unit;
        this.Nm_Unit = Nm_Unit;
    }

    public void setKd_Urusan(int Kd_Urusan){ this.Kd_Urusan = Kd_Urusan; }
    public void setKd_Bidang(int Kd_Bidang){ this.Kd_Bidang = Kd_Bidang; }
    public void setKd_Unit(int Kd_Unit){ this.Kd_Unit = Kd_Unit; }

    public void setNm_Unit(String Nm_Unit){ this.Nm_Unit = Nm_Unit; }

    public int getKd_Urusan(){ return this.Kd_Urusan; }
    public int getKd_Bidang(){ return this.Kd_Bidang; }
    public int getKd_Unit(){ return this.Kd_Unit; }

    public String getNm_Unit(){ return this.Nm_Unit; }

}
