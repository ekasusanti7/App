package datacenter.programmer_es.simda_apk;

/**
 * Created by Programmer_ES on 5/12/2017.
 */
public class LBidang {
    private int Kd_Urusan;
    private int Kd_Bidang;
    private String Nm_Bidang;
    public LBidang(){}
    public LBidang(int Kd_Urusan, int Kd_Bidang, String Nm_Bidang){
        this.Kd_Urusan = Kd_Urusan;
        this.Kd_Bidang = Kd_Bidang;
        this.Nm_Bidang = Nm_Bidang;
    }

    public void setKd_Urusan(int Kd_Urusan){ this.Kd_Urusan = Kd_Urusan; }
    public void setKd_Bidang(int Kd_Bidang){ this.Kd_Bidang = Kd_Bidang; }

    public void setNm_Bidang(String Nm_Bidang){ this.Nm_Bidang = Nm_Bidang; }

    public int getKd_Urusan(){ return this.Kd_Urusan; }
    public int getKd_Bidang(){ return this.Kd_Bidang; }

    public String getNm_Bidang(){ return this.Nm_Bidang; }

}
