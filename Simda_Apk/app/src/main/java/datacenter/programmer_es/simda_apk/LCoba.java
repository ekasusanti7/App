package datacenter.programmer_es.simda_apk;

/**
 * Created by Programmer_ES on 5/9/2017.
 */
public class LCoba {
    private int Kd_Urusan;
    private String Nm_Urusan;
    public LCoba(){}
    public LCoba(int Kd_Urusan, String Nm_Urusan){
        this.Kd_Urusan = Kd_Urusan;
        this.Nm_Urusan = Nm_Urusan;
    }

    public void setKd_Urusan(int Kd_Urusan){ this.Kd_Urusan = Kd_Urusan; }

    public void setNm_Urusan(String Nm_Urusan){ this.Nm_Urusan = Nm_Urusan; }

    public int getKd_Urusan(){
        return this.Kd_Urusan;
    }

    public String getNm_Urusan(){ return this.Nm_Urusan; }
}