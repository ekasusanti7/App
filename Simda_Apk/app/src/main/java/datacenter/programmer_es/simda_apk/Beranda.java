package datacenter.programmer_es.simda_apk;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Programmer_ES on 5/2/2017.
 */
public class Beranda extends AppCompatActivity  {
    private Context c;
    private DrawerLayout drawer;
    private WebView webview;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            webview = (WebView) findViewById(R.id.web_home);
            String content = getResources(getResources(), R.raw.homeapp);
            String mimeType ="text/html";
            String encoding ="UTF-8";
            webview.loadDataWithBaseURL("file:///android_asset", content, mimeType, encoding, null);
            c=getApplicationContext();

            Toolbar toolbar =(Toolbar) findViewById(R.id.inc_menuutama);
            setSupportActionBar(toolbar);
            toolbar.setTitleTextColor(getResources().getColor(R.color.putih));
            setToolbarTitle("Pembukuan Simda");
            drawer =(DrawerLayout) findViewById(R.id.sliding_layout);
            ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }

                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }
            };
            drawer.setDrawerListener(actionBarDrawerToggle);
            actionBarDrawerToggle.syncState();


            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(navView);
        }
        public static String getResources(Resources resources, int homeapp) {
            InputStream rawResource = resources.openRawResource(homeapp);
            String content = webToString(rawResource);
            try {
                rawResource.close();
            }catch (IOException e){

            }
            return content;
        }

        private static String webToString(InputStream rawResource) {
            String l;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(rawResource));
            StringBuilder stringBuilder = new StringBuilder();
            try {
                while ((l=bufferedReader.readLine())!=null){
                    stringBuilder.append(l+"\n");
                }
            } catch (IOException e){

            }
            return stringBuilder.toString();
        }
NavigationView.OnNavigationItemSelectedListener navView=new NavigationView.OnNavigationItemSelectedListener(){

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (item.isChecked()) item.setChecked(false);
        else item.setChecked(true);

        drawer.closeDrawers();
        switch (item.getItemId()){
            case R.id.nav_beranda:
                Intent home = new Intent(Beranda.this, Beranda.class);
                startActivity(home);
                return true;
            case R.id.nav_laporan:
                Intent laporan=new Intent(Beranda.this,Laporan.class);
                startActivity(laporan);
                return true;
            case R.id.nav_tatausaha:
                Intent tatausaha=new Intent(Beranda.this,TataUsaha.class);
                startActivity(tatausaha);
                return true;
            case R.id.nav_bantuan:
                Intent bantuan=new Intent(Beranda.this,Beranda.class);
                startActivity(bantuan);
                return true;
            case R.id.nav_tentang:
                Intent tentang=new Intent(Beranda.this,Beranda.class);
                startActivity(tentang);
                return true;
            case R.id.nav_keluar:
                dialogKeluar();
                return true;
            default:
                Toast.makeText(getApplicationContext(), "ERROR NAV", Toast.LENGTH_SHORT).show();
                return true;
        }
    }
};

        public void setToolbarTitle(String title) {
            getSupportActionBar().setTitle(title);
        }
        private void dialogKeluar() {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Apakah Anda Ingin Keluar?").setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface argO, int arg1) {
                            Beranda.this.finish();
                        }
                    }).setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    arg0.cancel();
                }
            }).show();
        }
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
}